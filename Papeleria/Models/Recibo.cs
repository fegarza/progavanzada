﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class Recibo
    {
        [Key]
        public int ReciboId { get; set; }

        [Required]
        public DateTime Fecha { get; set; }

        [Required]
        public double Total { get; set; }

        [Required]
        [Display(Name ="Sub total")]
        public double SubTotal { get; set; }


         public ICollection<Factura> Facturas { get; set; }


        public ICollection<ReciboDetalle> Detalles { get; set; }
    }
}