﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class Factura
    {
        [Key]
        public int FacturaId { get; set; }
        
        [Required]
        public int ReciboId { get; set; }

        public Recibo Recibo { get; set; }

        [Required]
        public DateTime Fecha { get; set; }

        //Datos del cliente
        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        [Required]
        public string Nombre { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(14)]
        [Required]
        public string RFC { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Direccion { get; set; }
        [Phone]
        public string Telefono { get; set; }



    }
}