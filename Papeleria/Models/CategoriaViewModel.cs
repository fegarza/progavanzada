﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class CategoriaViewModel
    {
        // Display Attribute will appear in the Html.LabelFor
        [Display(Name = "Categoria")]
        public int CategoriaId { get; set; }
        public IEnumerable<Categoria> Categorias { get; set; }
    }
}