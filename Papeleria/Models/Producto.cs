﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class Producto
    {
        [Key]
        public int ProductoId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Required]
        public string Titulo { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(120)]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(44)]
        [Required]
        [Display(Name = "Código de barras")]
        public string CodigoDeBarras { get; set; }
        [Required]
        public double Precio { get; set; }
 
        [Required]
        public int CategoriaId { get; set; }

        public Categoria Categoria { get; set; }


    }
}