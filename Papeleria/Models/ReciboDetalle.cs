﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class ReciboDetalle
    {
        [Key]
        [Column(Order = 1)]
        public int ReciboId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ProductoId { get; set; }       

        public Recibo Recibo { get; set; }
        public Producto Producto { get; set; }

        [Required]
        public int Cantidad { get; set; }

    }
}