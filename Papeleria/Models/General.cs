﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Papeleria.Models
{
    public class General
    {

        [Key]
        public int GeneralId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string RazonSocial { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Direccion { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(14)]
        [Required]
        public string RFC { get; set; }
    }
}