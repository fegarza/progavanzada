﻿using Papeleria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Papeleria.Core
{
    public class Contador
    {
        public static Contador ContadorSingleton;
        private ApplicationDbContext db = new ApplicationDbContext();



        public int productosCont = 0;
        public int categoriasCont = 0;
        public int ventasCont = 0;
        public int facturasCont = 0;


        public Dictionary<String, int> Contadores = new Dictionary<string, int>();


        public static Contador ObtenerInstancia()
        {
            if (ContadorSingleton == null)
            {
                ContadorSingleton = new Contador();
               
            }
            ContadorSingleton.CargarConteos();
            return ContadorSingleton;
        }


        public void CargarConteos()
        {
            this.productosCont = db.Productos.Count();
            this.categoriasCont = db.Categorias.Count();
            this.ventasCont = db.Recibos.Count();
            this.facturasCont = db.Facturas.Count();
        }


    }



}