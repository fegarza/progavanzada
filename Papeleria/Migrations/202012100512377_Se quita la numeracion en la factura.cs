namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sequitalanumeracionenlafactura : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Facturas", "Direccion", c => c.String(nullable: false, maxLength: 50, unicode: false));
            DropColumn("dbo.Facturas", "Numeracion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Facturas", "Numeracion", c => c.String(maxLength: 44, unicode: false));
            DropColumn("dbo.Facturas", "Direccion");
        }
    }
}
