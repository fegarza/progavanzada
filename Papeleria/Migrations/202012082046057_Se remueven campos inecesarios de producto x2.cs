namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seremuevencamposinecesariosdeproductox2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reciboes",
                c => new
                    {
                        ReciboId = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Total = c.Double(nullable: false),
                        SubTotal = c.Double(nullable: false),
                        IVA = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ReciboId);
            
            CreateTable(
                "dbo.ReciboDetalles",
                c => new
                    {
                        ReciboId = c.Int(nullable: false),
                        Consecutivo = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReciboId, t.Consecutivo })
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Reciboes", t => t.ReciboId, cascadeDelete: true)
                .Index(t => t.ReciboId)
                .Index(t => t.ProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReciboDetalles", "ReciboId", "dbo.Reciboes");
            DropForeignKey("dbo.ReciboDetalles", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.ReciboDetalles", new[] { "ProductoId" });
            DropIndex("dbo.ReciboDetalles", new[] { "ReciboId" });
            DropTable("dbo.ReciboDetalles");
            DropTable("dbo.Reciboes");
        }
    }
}
