namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Serelacionafacturaarecibo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FacturaDetalles", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.FacturaDetalles", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.FacturaDetalles", new[] { "FacturaId" });
            DropIndex("dbo.FacturaDetalles", new[] { "ProductoId" });
            AddColumn("dbo.Facturas", "ReciboId", c => c.Int(nullable: false));
            AddColumn("dbo.Facturas", "Nombre", c => c.String(nullable: false, maxLength: 40, unicode: false));
            AddColumn("dbo.Facturas", "Telefono", c => c.String());
            CreateIndex("dbo.Facturas", "ReciboId");
            AddForeignKey("dbo.Facturas", "ReciboId", "dbo.Reciboes", "ReciboId", cascadeDelete: true);
            DropColumn("dbo.Facturas", "Total");
            DropColumn("dbo.Facturas", "SubTotal");
            DropColumn("dbo.Facturas", "IVA");
            DropTable("dbo.FacturaDetalles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FacturaDetalles",
                c => new
                    {
                        FacturaId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FacturaId, t.ProductoId });
            
            AddColumn("dbo.Facturas", "IVA", c => c.Double(nullable: false));
            AddColumn("dbo.Facturas", "SubTotal", c => c.Double(nullable: false));
            AddColumn("dbo.Facturas", "Total", c => c.Double(nullable: false));
            DropForeignKey("dbo.Facturas", "ReciboId", "dbo.Reciboes");
            DropIndex("dbo.Facturas", new[] { "ReciboId" });
            DropColumn("dbo.Facturas", "Telefono");
            DropColumn("dbo.Facturas", "Nombre");
            DropColumn("dbo.Facturas", "ReciboId");
            CreateIndex("dbo.FacturaDetalles", "ProductoId");
            CreateIndex("dbo.FacturaDetalles", "FacturaId");
            AddForeignKey("dbo.FacturaDetalles", "ProductoId", "dbo.Productoes", "ProductoId", cascadeDelete: true);
            AddForeignKey("dbo.FacturaDetalles", "FacturaId", "dbo.Facturas", "FacturaId", cascadeDelete: true);
        }
    }
}
