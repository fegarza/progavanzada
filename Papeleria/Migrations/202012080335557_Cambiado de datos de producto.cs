namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiadodedatosdeproducto : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Productoes", "Categoria_CategoriaId", "dbo.Categorias");
            DropIndex("dbo.Productoes", new[] { "Categoria_CategoriaId" });
            AlterColumn("dbo.Categorias", "Titulo", c => c.String(nullable: false, maxLength: 30, unicode: false));
            AlterColumn("dbo.Facturas", "RFC", c => c.String(nullable: false, maxLength: 14, unicode: false));
            AlterColumn("dbo.Productoes", "Titulo", c => c.String(nullable: false, maxLength: 30, unicode: false));
            AlterColumn("dbo.Productoes", "CodigoDeBarras", c => c.String(nullable: false, maxLength: 44, unicode: false));
            AlterColumn("dbo.Productoes", "Categoria_CategoriaId", c => c.Int(nullable: false));
            AlterColumn("dbo.Generals", "RazonSocial", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.Generals", "Direccion", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.Generals", "RFC", c => c.String(nullable: false, maxLength: 14, unicode: false));
            CreateIndex("dbo.Productoes", "Categoria_CategoriaId");
            AddForeignKey("dbo.Productoes", "Categoria_CategoriaId", "dbo.Categorias", "CategoriaId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productoes", "Categoria_CategoriaId", "dbo.Categorias");
            DropIndex("dbo.Productoes", new[] { "Categoria_CategoriaId" });
            AlterColumn("dbo.Generals", "RFC", c => c.String(maxLength: 14, unicode: false));
            AlterColumn("dbo.Generals", "Direccion", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Generals", "RazonSocial", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Productoes", "Categoria_CategoriaId", c => c.Int());
            AlterColumn("dbo.Productoes", "CodigoDeBarras", c => c.String(maxLength: 44, unicode: false));
            AlterColumn("dbo.Productoes", "Titulo", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.Facturas", "RFC", c => c.String(maxLength: 14, unicode: false));
            AlterColumn("dbo.Categorias", "Titulo", c => c.String(maxLength: 30, unicode: false));
            CreateIndex("dbo.Productoes", "Categoria_CategoriaId");
            AddForeignKey("dbo.Productoes", "Categoria_CategoriaId", "dbo.Categorias", "CategoriaId");
        }
    }
}
