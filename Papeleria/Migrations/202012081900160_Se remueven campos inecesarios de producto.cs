namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seremuevencamposinecesariosdeproducto : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Productoes", "Stock");
            DropColumn("dbo.Productoes", "MinStock");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productoes", "MinStock", c => c.Int(nullable: false));
            AddColumn("dbo.Productoes", "Stock", c => c.Int(nullable: false));
        }
    }
}
