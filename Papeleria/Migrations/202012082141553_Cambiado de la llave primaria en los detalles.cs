namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiadodelallaveprimariaenlosdetalles : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.FacturaDetalles");
            DropPrimaryKey("dbo.ReciboDetalles");
            AddPrimaryKey("dbo.FacturaDetalles", new[] { "FacturaId", "ProductoId" });
            AddPrimaryKey("dbo.ReciboDetalles", new[] { "ReciboId", "ProductoId" });
            DropColumn("dbo.FacturaDetalles", "Consecutivo");
            DropColumn("dbo.ReciboDetalles", "Consecutivo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReciboDetalles", "Consecutivo", c => c.Int(nullable: false));
            AddColumn("dbo.FacturaDetalles", "Consecutivo", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.ReciboDetalles");
            DropPrimaryKey("dbo.FacturaDetalles");
            AddPrimaryKey("dbo.ReciboDetalles", new[] { "ReciboId", "Consecutivo" });
            AddPrimaryKey("dbo.FacturaDetalles", new[] { "FacturaId", "Consecutivo" });
        }
    }
}
