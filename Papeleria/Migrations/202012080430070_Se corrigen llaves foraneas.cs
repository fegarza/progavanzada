namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Secorrigenllavesforaneas : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Productoes", name: "Categoria_CategoriaId", newName: "CategoriaId");
            RenameIndex(table: "dbo.Productoes", name: "IX_Categoria_CategoriaId", newName: "IX_CategoriaId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Productoes", name: "IX_CategoriaId", newName: "IX_Categoria_CategoriaId");
            RenameColumn(table: "dbo.Productoes", name: "CategoriaId", newName: "Categoria_CategoriaId");
        }
    }
}
