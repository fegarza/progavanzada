namespace Papeleria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sequitaelivaderecibo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reciboes", "IVA");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reciboes", "IVA", c => c.Double(nullable: false));
        }
    }
}
