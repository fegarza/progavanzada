﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Papeleria.Models;

namespace Papeleria.Controllers
{
    public class ReciboDetallesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReciboDetalles
        public ActionResult Index()
        {
            var recibosDetalles = db.RecibosDetalles.Include(r => r.Producto).Include(r => r.Recibo);
            return View(recibosDetalles.ToList());
        }

        // GET: ReciboDetalles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReciboDetalle reciboDetalle = db.RecibosDetalles.Find(id);
            if (reciboDetalle == null)
            {
                return HttpNotFound();
            }
            return View(reciboDetalle);
        }

        // GET: ReciboDetalles/Create
        public ActionResult Create()
        {
            ViewBag.ProductoId = new SelectList(db.Productos, "ProductoId", "Titulo");
            ViewBag.ReciboId = new SelectList(db.Recibos, "ReciboId", "ReciboId");
            return View();
        }

        // POST: ReciboDetalles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReciboId,ProductoId,Cantidad")] ReciboDetalle reciboDetalle)
        {
            if (ModelState.IsValid)
            {
                db.RecibosDetalles.Add(reciboDetalle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductoId = new SelectList(db.Productos, "ProductoId", "Titulo", reciboDetalle.ProductoId);
            ViewBag.ReciboId = new SelectList(db.Recibos, "ReciboId", "ReciboId", reciboDetalle.ReciboId);
            return View(reciboDetalle);
        }

        // GET: ReciboDetalles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReciboDetalle reciboDetalle = db.RecibosDetalles.Find(id);
            if (reciboDetalle == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductoId = new SelectList(db.Productos, "ProductoId", "Titulo", reciboDetalle.ProductoId);
            ViewBag.ReciboId = new SelectList(db.Recibos, "ReciboId", "ReciboId", reciboDetalle.ReciboId);
            return View(reciboDetalle);
        }

        // POST: ReciboDetalles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReciboId,ProductoId,Cantidad")] ReciboDetalle reciboDetalle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reciboDetalle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductoId = new SelectList(db.Productos, "ProductoId", "Titulo", reciboDetalle.ProductoId);
            ViewBag.ReciboId = new SelectList(db.Recibos, "ReciboId", "ReciboId", reciboDetalle.ReciboId);
            return View(reciboDetalle);
        }

        // GET: ReciboDetalles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReciboDetalle reciboDetalle = db.RecibosDetalles.Find(id);
            if (reciboDetalle == null)
            {
                return HttpNotFound();
            }
            return View(reciboDetalle);
        }

        // POST: ReciboDetalles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReciboDetalle reciboDetalle = db.RecibosDetalles.Find(id);
            db.RecibosDetalles.Remove(reciboDetalle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
