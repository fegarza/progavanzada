﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Papeleria.Models;

namespace Papeleria.Controllers
{
    public class RecibosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Recibos
        public ActionResult Index()
        {
            return View(db.Recibos.Include(i=>i.Facturas).ToList());
        }

        // GET: Recibos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recibo recibo = db.Recibos.Find(id);

            ViewBag.Detalles = db.RecibosDetalles.Include(i => i.Producto).Where(w => w.ReciboId == id).ToList();


            if (recibo == null)
            {
                return HttpNotFound();
            }
            return View(recibo);
        }

        // GET: Recibos/Create
        public ActionResult Create()
        {
            Session["Carrito"] = new List<ReciboDetalle>();
            ViewBag.Carrito = new List<ReciboDetalle>() {
                new ReciboDetalle(){ProductoId = 1, Producto = new Producto(){ Titulo = "A"} }
            };

            return View();
        }

        // POST: Recibos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReciboId,Fecha,Total,SubTotal")] Recibo recibo)
        {



            if (ModelState.IsValid)
            {


                if (Session["Carrito"] != null && ((List<ReciboDetalle>)Session["Carrito"]).Count() > 0)
                {

                    recibo.SubTotal = 0;
                    recibo.Total = 0;
                    foreach (ReciboDetalle r in ((List<ReciboDetalle>)Session["Carrito"]))
                    {
                        recibo.SubTotal += r.Cantidad * r.Producto.Precio;
                    }
                    recibo.Total = (recibo.SubTotal * .16) + recibo.SubTotal;
                    recibo.Fecha = DateTime.Now;
                    // recibo.
                    db.Recibos.Add(recibo);
                    db.SaveChanges();
                    foreach (ReciboDetalle r in ((List<ReciboDetalle>)Session["Carrito"]))
                    {
                        r.Producto = null;
                        r.ReciboId = recibo.ReciboId;
                        db.RecibosDetalles.Add(r);
                        db.SaveChanges();
                    }
                }




                return RedirectToAction("Index");
            }

            return View(recibo);
        }

        // GET: Recibos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recibo recibo = db.Recibos.Find(id);
            if (recibo == null)
            {
                return HttpNotFound();
            }
            return View(recibo);
        }

        // POST: Recibos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReciboId,Fecha,Total,SubTotal,IVA")] Recibo recibo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recibo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(recibo);
        }

        // GET: Recibos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recibo recibo = db.Recibos.Find(id);
            if (recibo == null)
            {
                return HttpNotFound();
            }
            return View(recibo);
        }





        [HttpGet, ActionName("EliminarDetalle")]
        public ActionResult EliminarDetalle(string ProductoId)
        {

            ReciboDetalle result = ((List<ReciboDetalle>)Session["Carrito"]).SingleOrDefault(s => s.ProductoId == int.Parse(ProductoId));
            if (result != null)
            {
                ((List<ReciboDetalle>)Session["Carrito"]).Remove(result);
            }
            return Json(ProductoId, JsonRequestBehavior.AllowGet);
        }


        [HttpGet, ActionName("IncrementarDetalle")]
        public ActionResult IncrementarDetalle(string ProductoId)
        {
            ReciboDetalle result = ((List<ReciboDetalle>)Session["Carrito"]).SingleOrDefault(s => s.ProductoId == int.Parse(ProductoId));
            if (result != null)
            {
                result.Cantidad++;
            }
            return Json(new { cantidad = result.Cantidad }
           , JsonRequestBehavior.AllowGet);
        }

        [HttpGet, ActionName("DecrementarDetalle")]
        public ActionResult DecrementarDetalle(string ProductoId)
        {
            ReciboDetalle result = ((List<ReciboDetalle>)Session["Carrito"]).SingleOrDefault(s => s.ProductoId == int.Parse(ProductoId));
            if (result != null)
            {
                result.Cantidad--;

                if (result.Cantidad == 0)
                {
                    ((List<ReciboDetalle>)Session["Carrito"]).Remove(result);
                }
            }
            int c = result.Cantidad;
            return Json(new { cantidad = c }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet, ActionName("AgregarDetalle")]
        public ActionResult AgregarDetalle(string Codigo)
        {
            if (Codigo != null)
            {
              


                List<Producto> pro =
                    (
                        from s in db.Productos
                        where s.CodigoDeBarras == Codigo
                        select s
                    ).ToList();



                if (pro != null && pro.Count > 0)
                {
                    if (Session["Carrito"] == null)
                    {
                        Session["Carrito"] = new List<ReciboDetalle>();

                    }
                    ReciboDetalle result = ((List<ReciboDetalle>)Session["Carrito"]).SingleOrDefault(s => s.Producto.CodigoDeBarras == Codigo);
                    if (result == null)
                    {
                        ((List<ReciboDetalle>)Session["Carrito"]).Add(new ReciboDetalle() { Producto = pro[0], ProductoId = pro[0].ProductoId, Cantidad = 1 });
                        return Json(pro[0], JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { existe = "si" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { existe = "si" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { existe = "si" }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpGet, ActionName("AgregarFactura")]
        public ActionResult AgregarFactura(string reciboId, string nombre, string rfc, string direccion, string telefono)
        {
            if (reciboId != null && nombre != null && rfc != null && direccion != null && telefono != null)
            {


                int recibox = int.Parse(reciboId);


                List<Factura> misFacturas = db.Facturas.Where(w => w.ReciboId == recibox).ToList();

                if(misFacturas.Count > 0)
                {
                    return Json(new { bien = "si", facturaId = misFacturas[0].FacturaId }, JsonRequestBehavior.AllowGet);

                }



                Factura nueva = new Factura()
                {
                    ReciboId = int.Parse(reciboId),
                    Nombre = nombre,
                    Telefono = telefono,
                    RFC = rfc,
                    Direccion = direccion,
                    Fecha = DateTime.Now
                };
                db.Facturas.Add(nueva);

                db.SaveChanges();

                return Json(new { bien = "si", facturaId = nueva.FacturaId }, JsonRequestBehavior.AllowGet);


            }
            else
            {
                return Json(new { bien = "no" }, JsonRequestBehavior.AllowGet);
            }

        }



        // POST: Recibos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recibo recibo = db.Recibos.Find(id);
            db.Recibos.Remove(recibo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
