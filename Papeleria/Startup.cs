﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Papeleria.Startup))]
namespace Papeleria
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
